В папке [terraform](https://gitlab.com/Keeper521/diplom/-/tree/main/terraform) лежат настройки инфраструктуры в yandex cloud. Разворачивается 3 сети, кластер kubernetes, создается сервисный аккаунт для работы с kubernetes и 3 ноды. Предварительно надо создать S3 bucket для записи терраформом изменений.
Для подключения к кластеру необходимо выполнить комманду:
```
yc managed-kubernetes cluster get-credentials <идентификатор или имя кластера> --external
```
В папке [kubernetes](https://gitlab.com/Keeper521/diplom/-/tree/main/kubernetes) лежит стэк для мониторинга kubernetes разворачиваемый через helm  
Установка стека производится командой:
```
helm install monitoring .diplom/kubernetes/kube-prometheus-stack/
```
Когда все успешно развернется то grafana будет доступна на порту 30100 ноды по внешнему ip который присвоится, посмотреть на какой ноде находится grafana можно по команде:
```
kubectl get po -o wide
```
Далее тестовое приложение развернется при любом коммите в проекте [devops-netology](https://gitlab.com/Keeper521/devops-netology). Предварительно необходимо изменить идентификатор или имя кластера в 30 строке файла [.gitlab-ci.yml](https://gitlab.com/Keeper521/devops-netology/-/blob/main/.gitlab-ci.yml)

Docker образы размещаются в репозиторий https://hub.docker.com/repository/docker/keeper521/nginx  
Приложение будет доступно по порту 31500.
Создание workspace в terraform происходит коммандой:
```
terraform workspace new stage
```
Просмотр и выбор рабочего workspace производится коммандами:
```
terraform workspace list
terraform workspace select stage
```
grafana доступна по адресу: http://51.250.35.89:30100   
логин: admin   
пароль: prom-operator  

приложение доступно по адресу:   http://84.201.132.148:31500
