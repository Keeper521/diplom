helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update

helm install monitoring prometheus-community/kube-prometheus-stack
helm install monitoring .

helm uninstall monitoring

Grafana
admin
prom-operator

Grafana port 30100
Html port 31500
